package org.hnau.api.common.endpoint

import org.hnau.base.data.bytes.adapter.BytesAdapter
import org.hnau.base.data.bytes.adapter.map
import org.hnau.base.data.bytes.adapter.short
import org.hnau.base.data.mapper.Mapper


data class EndpointKey(
        val value: Short
) {

    companion object {

        val shortToEndpointKeyMapper =
                Mapper(::EndpointKey, EndpointKey::value)

        val bytesAdapter =
                BytesAdapter.short.map(shortToEndpointKeyMapper)

    }

    override fun toString() = value.toString()

}