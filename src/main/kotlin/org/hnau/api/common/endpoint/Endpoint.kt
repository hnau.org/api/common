package org.hnau.api.common.endpoint


data class Endpoint<I, O>(
        val key: EndpointKey,
        val name: String
)