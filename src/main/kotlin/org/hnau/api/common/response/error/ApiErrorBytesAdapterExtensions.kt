package org.hnau.api.common.response.error

import org.hnau.base.data.bytes.adapter.BytesAdapter
import org.hnau.base.data.bytes.adapter.TypedBytesAdapter
import org.hnau.base.data.bytes.adapter.short
import org.hnau.base.data.bytes.adapter.typed


fun ApiError.Companion.bytesAdapter(
        typedAdapters: Map<Short, TypedBytesAdapter<out ApiError>>
) = BytesAdapter.typed(
        typedAdapters = typedAdapters,
        keyAdapter = BytesAdapter.short
)