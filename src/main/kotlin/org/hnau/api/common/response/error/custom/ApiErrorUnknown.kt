package org.hnau.api.common.response.error.custom

import org.hnau.api.common.response.error.ApiError
import org.hnau.base.data.bytes.adapter.BytesAdapter
import org.hnau.base.data.bytes.adapter.stateless
import org.hnau.base.utils.Empty


class ApiErrorUnknown : Empty(), ApiError {

    companion object {

        val bytesAdapter =
                BytesAdapter.stateless(::ApiErrorUnknown)

    }

}