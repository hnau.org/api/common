package org.hnau.api.common.response

import org.hnau.api.common.response.error.ApiError


sealed class ApiResponse<out T> {

    companion object {}

    data class Success<T>(
            val value: T
    ) : ApiResponse<T>()

    data class Error(
            val error: ApiError
    ) : ApiResponse<Nothing>()

}