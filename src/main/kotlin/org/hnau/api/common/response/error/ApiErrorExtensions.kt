package org.hnau.api.common.response.error


val ApiError.exception
    get() = ApiException(this)