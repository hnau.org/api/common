package org.hnau.api.common.response.error.custom

import org.hnau.api.common.response.error.ApiError
import org.hnau.base.data.bytes.adapter.BytesAdapter
import org.hnau.base.data.bytes.adapter.map
import org.hnau.base.data.mapper.Mapper
import org.hnau.base.utils.validator.error.ValidatingError


data class ApiErrorValidating(
        val error: ValidatingError
) : ApiError {

    companion object {

        val validatingErrorToApiErrorValidatingMapper =
                Mapper(::ApiErrorValidating, ApiErrorValidating::error)

        fun bytesAdapter(
                validatingErrorBytesAdapter: BytesAdapter<ValidatingError>
        ) = validatingErrorBytesAdapter
                .map(validatingErrorToApiErrorValidatingMapper)


    }

}