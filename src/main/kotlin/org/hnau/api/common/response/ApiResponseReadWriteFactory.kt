package org.hnau.api.common.response

import org.hnau.api.common.response.error.ApiError
import org.hnau.base.data.bytes.adapter.BytesAdapter
import org.hnau.base.data.bytes.provider.BytesProvider
import org.hnau.base.data.bytes.provider.readBoolean
import org.hnau.base.data.bytes.receiver.BytesReceiver
import org.hnau.base.data.bytes.receiver.write
import org.hnau.base.data.bytes.receiver.writeBoolean
import org.hnau.base.extensions.boolean.checkBoolean


class ApiResponseReadWriteFactory(
        @PublishedApi
        internal val apiErrorBytesAdapter: BytesAdapter<ApiError>
) {

    inline fun <T> read(
            noinline bytesProvider: BytesProvider,
            contentReader: BytesProvider.() -> T
    ) = bytesProvider.readBoolean().checkBoolean(
            ifTrue = { ApiResponse.Success(contentReader(bytesProvider)) },
            ifFalse = { ApiResponse.Error(apiErrorBytesAdapter.read(bytesProvider)) }
    )

    inline fun <T> write(
            apiResponse: ApiResponse<T>,
            noinline bytesReceiver: BytesReceiver,
            contentWriter: BytesReceiver.(T) -> Unit
    ) = when (apiResponse) {
        is ApiResponse.Success -> {
            write(
                    success = true,
                    bytesReceiver = bytesReceiver,
                    writeContent = { contentWriter(apiResponse.value) }
            )
        }
        is ApiResponse.Error -> {
            writeError(
                    bytesReceiver = bytesReceiver,
                    error = apiResponse.error
            )
        }
    }

    inline fun write(
            success: Boolean,
            noinline bytesReceiver: BytesReceiver,
            writeContent: BytesReceiver.() -> Unit
    ) {
        bytesReceiver.writeBoolean(success)
        bytesReceiver.writeContent()
    }

    fun writeError(
            bytesReceiver: BytesReceiver,
            error: ApiError
    ) = write(
            success = false,
            bytesReceiver = bytesReceiver
    ) {
        write(error, apiErrorBytesAdapter.write)
    }

}

fun ApiResponse.Companion.readWriteFactory(
        apiErrorBytesAdapter: BytesAdapter<ApiError>
) = ApiResponseReadWriteFactory(
        apiErrorBytesAdapter = apiErrorBytesAdapter
)