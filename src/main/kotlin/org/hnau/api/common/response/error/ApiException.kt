package org.hnau.api.common.response.error


class ApiException(
        val error: ApiError
) : RuntimeException(
        error.toString()
) {

    override fun toString() = error.toString()

    override fun equals(other: Any?) =
            other is ApiException && other.error == error || super.equals(other)

    override fun hashCode() = error.hashCode()

}