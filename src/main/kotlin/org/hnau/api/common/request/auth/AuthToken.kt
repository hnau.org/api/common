package org.hnau.api.common.request.auth

import org.hnau.base.data.bytes.adapter.BytesAdapter
import org.hnau.base.data.bytes.adapter.map
import org.hnau.base.data.mapper.Mapper
import org.hnau.base.data.mapper.plus
import org.hnau.base.extensions.uuid.bytesToUUID
import org.hnau.base.extensions.uuid.compactStringToUUID
import org.hnau.base.extensions.uuid.uuid
import java.util.*


data class AuthToken(
        val value: UUID
) {

    companion object {

        val uuidToAuthTokenMapper =
                Mapper(::AuthToken, AuthToken::value)

        val bytesToAuthTokenMapper =
                Mapper.bytesToUUID + uuidToAuthTokenMapper

        val stringToAuthTokenMapper =
                Mapper.compactStringToUUID + uuidToAuthTokenMapper

        val bytesAdapter =
                BytesAdapter.uuid.map(uuidToAuthTokenMapper)

    }

    override fun toString() = "AuthToken(***)"

}