package org.hnau.api.common.response

import org.assertj.core.api.Assertions.assertThat
import org.hnau.api.common.response.error.custom.ApiErrorUnknown
import org.hnau.api.common.response.error.custom.ApiErrorValidating
import org.hnau.base.data.bytes.adapter.BytesAdapter
import org.hnau.base.data.bytes.adapter.short
import org.hnau.base.data.bytes.adapter.toTypedAdapter
import org.hnau.base.data.bytes.adapter.typed
import org.hnau.base.data.bytes.provider.BufferedBytesProvider
import org.hnau.base.data.bytes.provider.BytesProvider
import org.hnau.base.data.bytes.provider.readString
import org.hnau.base.data.bytes.receiver.BufferedBytesReceiver
import org.hnau.base.data.bytes.receiver.BytesReceiver
import org.hnau.base.data.bytes.receiver.writeString
import org.hnau.base.data.locale.LocaleStructureValidatingError
import org.hnau.base.utils.base64.Base64ValidatingError
import org.hnau.base.utils.validator.CompositeValidatingError
import org.hnau.base.utils.validator.EmailStructoreValidatingError
import org.hnau.base.utils.validator.VariantsValidatingError
import org.hnau.base.utils.validator.charsequence.IncorrectCharValidatingError
import org.hnau.base.utils.validator.error.ValidatingError
import org.hnau.base.utils.validator.size.MaxLengthValidatingError
import org.hnau.base.utils.validator.size.MinLengthValidatingError
import org.junit.Test


class ApiResponseTests {

    @Test
    fun test() {

        lateinit var validatingErrorBytesAdapter: BytesAdapter<ValidatingError>
        validatingErrorBytesAdapter = BytesAdapter.typed(
                typedAdapters = mapOf(
                        0.toShort() to MaxLengthValidatingError.bytesAdapter.toTypedAdapter(),
                        1.toShort() to MinLengthValidatingError.bytesAdapter.toTypedAdapter(),
                        2.toShort() to Base64ValidatingError.bytesAdapter.toTypedAdapter(),
                        3.toShort() to CompositeValidatingError.bytesAdapter { validatingErrorBytesAdapter }.toTypedAdapter(),
                        4.toShort() to EmailStructoreValidatingError.bytesAdapter.toTypedAdapter(),
                        5.toShort() to IncorrectCharValidatingError.bytesAdapter.toTypedAdapter(),
                        6.toShort() to LocaleStructureValidatingError.bytesAdapter.toTypedAdapter(),
                        7.toShort() to VariantsValidatingError.bytesAdapter.toTypedAdapter()
                ),
                keyAdapter = BytesAdapter.short

        )

        val apiErrorBytesAdapter =
                BytesAdapter.typed(
                        typedAdapters = mapOf(
                                0.toShort() to ApiErrorValidating.bytesAdapter(validatingErrorBytesAdapter).toTypedAdapter(),
                                1.toShort() to ApiErrorUnknown.bytesAdapter.toTypedAdapter()
                        ),
                        keyAdapter = BytesAdapter.short
                )

        val apiResponseReadWriteFactory =
                ApiResponse.readWriteFactory(apiErrorBytesAdapter)

        val apiResponse1 = ApiResponse.Success("123")
        val apiResponse2 = ApiResponse.Error(ApiErrorValidating(IncorrectCharValidatingError(3, '^')))

        val bytes = BufferedBytesReceiver()
                .apply {
                    apiResponseReadWriteFactory.write(apiResponse1, this, BytesReceiver::writeString)
                    apiResponseReadWriteFactory.write(apiResponse2, this, BytesReceiver::writeString)
                }
                .buffer

        val bytesProvider = BufferedBytesProvider(bytes)
        val apiResponse1Decoded = apiResponseReadWriteFactory.read(bytesProvider, BytesProvider::readString)
        val apiResponse2Decoded = apiResponseReadWriteFactory.read(bytesProvider, BytesProvider::readString)

        assertThat(bytesProvider.available).isEqualTo(0)

        assertThat(apiResponse1Decoded).isEqualTo(apiResponse1)
        assertThat(apiResponse2Decoded).isEqualTo(apiResponse2)


    }

}